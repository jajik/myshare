package net.chlup.myshare;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.Menu;
import android.view.MenuItem;

public class myshare extends Activity {

   /* Prefs */
   Preferences prefs;

   /** Called when the activity is first created. */
   @Override
   public void onCreate(Bundle savedInstanceState) {
      /* Get prefs */
      this.prefs = new Preferences(this);
      this.prefs.load();

      //setTheme(android.R.style.Theme_DeviceDefault);
      if (!this.prefs.use_default_theme) {
         if (this.prefs.dark_mode) {
            setTheme(android.R.style.Theme_Material);
         } else {
            setTheme(android.R.style.Theme_Material_Light);
         }
      }

      setContentView(R.layout.main);
      super.onCreate(savedInstanceState);
   }

   @Override
   protected void onStart()   {
      super.onStart();

      if(this.prefs.target_urls.get(0) != null) {

         Intent intent = getIntent();
         Bundle extras = intent.getExtras();
         String action = intent.getAction();

         if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
               ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            }
         }

         if (Intent.ACTION_SEND.equals(action) || Intent.ACTION_SEND_MULTIPLE.equals(action)) {
            Intent intentservice=new Intent(this, myshareservice.class);
            intentservice.setAction(action);
            intentservice.putExtras(extras);
            /* co bylo vybrano za server? */
            int serverIndex = 0;
            intentservice.putExtra("MYSHARE_SERVERINDEX", serverIndex);
            /* co bylo vybrano za znacky? */
            String[] tags = {};
            intentservice.putExtra("MYSHARE_TAGS", tags);
            startService(intentservice);
            this.finish();
         }

      }

   }

   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      menu.add(0, 100, 100, R.string.mainSettings).setIcon(
            android.R.drawable.ic_menu_preferences);
      return true;
   }
   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      switch (item.getItemId()) {
      case 100:
         Intent prefA = new Intent(this, PreferencesActivity.class);
         startActivityForResult(prefA, 0);
         break;
      }

      return true;
   }
   @Override
   public void onActivityResult(int req, int res, Intent i) {
      this.prefs.load();
   }

}
