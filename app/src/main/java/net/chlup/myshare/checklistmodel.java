package net.chlup.myshare;

public class checklistmodel {
   private String label;
   private String sublabel;
   private boolean selected;

   public checklistmodel(String label, String sublabel) {
       this.label = label;
       this.sublabel = sublabel;
       selected = false;
   }

   public String getLabel() {
       return label;
   }

   public String getSublabel() {
       return sublabel;
   }

   public void setLabel(String label) {
       this.label = label;
   }

   public void setSublabel(String sublabel) {
       this.sublabel = sublabel;
   }

   public boolean isSelected() {
       return selected;
   }

   public void setSelected(boolean selected) {
       this.selected = selected;
   }  
}
