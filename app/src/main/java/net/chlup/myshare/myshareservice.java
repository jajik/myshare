package net.chlup.myshare;

import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.config.Registry;
import cz.msebera.android.httpclient.config.RegistryBuilder;
import cz.msebera.android.httpclient.conn.HttpClientConnectionManager;
import cz.msebera.android.httpclient.conn.socket.ConnectionSocketFactory;
import cz.msebera.android.httpclient.conn.ssl.SSLConnectionSocketFactory;
import cz.msebera.android.httpclient.entity.ContentType;
import cz.msebera.android.httpclient.entity.mime.HttpMultipartMode;
import cz.msebera.android.httpclient.entity.mime.MultipartEntityBuilder;
import cz.msebera.android.httpclient.entity.mime.content.InputStreamBody;
import cz.msebera.android.httpclient.entity.mime.content.StringBody;
import cz.msebera.android.httpclient.impl.client.DefaultHttpRequestRetryHandler;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;
import cz.msebera.android.httpclient.impl.conn.BasicHttpClientConnectionManager;
import cz.msebera.android.httpclient.ssl.SSLContextBuilder;
import cz.msebera.android.httpclient.util.EntityUtils;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Build;
import android.os.Environment;
import android.os.PowerManager;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;

public class myshareservice extends IntentService {

   /* Prefs */
   Preferences prefs;
   HttpClient httpclient = null;

   /* Storage PATH */
   String storagepath = Environment.getExternalStorageDirectory().toString();

   /* Notification */
   NotificationManager notman;
   PendingIntent notintent;
   
   int sendOK;
   int sendError;

   public myshareservice() {
      super("myshareservice");
   }



   @Override
   protected void onHandleIntent(Intent intent) {

      /* Get prefs */
      this.prefs = new Preferences(this);
      this.prefs.load();

      setupHttpConnection();

      /* nedovol, aby usnulo CPU behem  uploadu (pokud to sem nedam a usnuti dovolim, fungovalo to pekne, ale pri usnuti telefonu se upload prerusil a bezel az po jeho probuzeni) */
      PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
      PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "myshareserviceStart: CPU no sleep");
      wl.acquire();
      myshareserviceStart(intent);
      wl.release();

   }

   private void setupHttpConnection() {
      HttpClientBuilder builder = HttpClientBuilder.create();
      builder.setRetryHandler(new DefaultHttpRequestRetryHandler(1, false)); // retry jen 1x (napriklad je nedostupna sit) (pokud bych to neuvedl, tak defaultne je tusim retry 3x)
      if (this.prefs.own_key_store) {
         try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            FileInputStream instream = new FileInputStream(new File(storagepath + "/security/cacerts.bks"));
            trustStore.load(instream, "changeit".toCharArray());
            instream.close();
            
            TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            trustManagerFactory.init(trustStore);

            SSLContext sslContext = SSLContextBuilder.create().build();
            sslContext.init(null, trustManagerFactory.getTrustManagers(), null);
            SSLConnectionSocketFactory socketFactory = new SSLConnectionSocketFactory(sslContext);

            Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory>create()
                    .register("https", socketFactory).build();
            HttpClientConnectionManager cm = new BasicHttpClientConnectionManager(registry);
            this.httpclient = builder.setSSLSocketFactory(socketFactory).setConnectionManager(cm)
                    .setConnectionTimeToLive(1, TimeUnit.MINUTES).build();
         }
         catch (KeyStoreException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            httpclient = null;
         }
         catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            httpclient = null;
         }
         catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            httpclient = null;
         }
         catch (CertificateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            httpclient = null;
         }
         catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            httpclient = null;
         }
         catch (KeyManagementException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            httpclient = null;
         }
      } else {
         this.httpclient = builder.setConnectionTimeToLive(1, TimeUnit.MINUTES).build();
      }
   }

   private void setupNotif() {
      /* Show state in notif bar */

      if (this.notman != null) {
         /* Remove notification */
         this.notman.cancelAll();
         this.notman = null;
      }

      /* Set up a persistent notification */
      this.notman = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
      this.notman.cancelAll();
      this.notintent = PendingIntent.getActivity(
            this,
            0,
            new Intent(this, myshare.class),
            0
      );
   }

   private void myshareserviceStart(Intent intent) {

      Bundle extras = intent.getExtras();
      String action = intent.getAction();
      int serverIndex = extras.getInt("MYSHARE_SERVERINDEX");

      if(this.prefs.target_urls.get(serverIndex) != null) {
         String result_last = "Error: unknown";
         this.sendOK=0;
         this.sendError=0;
         setupNotif();
         setNotif(0, 0, myshareservice.this.getString(R.string.msgUploadStart), 0);
         // if this is from the share menu
         if (Intent.ACTION_SEND.equals(action)) {
            if (extras.containsKey(Intent.EXTRA_STREAM)) {
               Log.i(this.getClass().getName(), "upload one file");
               // Get resource path from intent callee
               Uri uri = (Uri) extras.getParcelable(Intent.EXTRA_STREAM);
               String result = mainSendRequest(intent, uri, serverIndex, extras.getStringArray("MYSHARE_TAGS"), false);
               setNotif(0, 0, result, 1);
               result_last = result;
            }
            else if (extras.containsKey(Intent.EXTRA_TEXT)) {
               Log.i(this.getClass().getName(), "upload one text");
               String result = mainSendRequestText(intent, serverIndex, extras.getStringArray("MYSHARE_TAGS"));
               setNotif(0, 0, result, 1);
               result_last = result;
            }
         }
         // if this is from the share menu
         else if (Intent.ACTION_SEND_MULTIPLE.equals(action)) {
            if (extras.containsKey(Intent.EXTRA_STREAM)) {
               Log.i(this.getClass().getName(), "upload list files");
               ArrayList<Uri> theFileList = intent.getParcelableArrayListExtra(Intent.EXTRA_STREAM);

               int id = 0;
               for(Uri uri: theFileList) {
                  String result = mainSendRequest(intent, uri, serverIndex, extras.getStringArray("MYSHARE_TAGS"), true);
                  setNotif(id, theFileList.size()-1, result, 1);
                  result_last = result;
                  ++id;
               }
            }
         }
         setNotif(0, 0, result_last, 2);
      }

   }

   private String mainSendRequestText(Intent intent, int serverIndex, String[] tags) {
      String result = "Error: unknown";
      InputStream is;
      String sharedText = "";
      if(intent.getStringExtra(Intent.EXTRA_TEXT) != null) {
         sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
      }
      try {
         is = new ByteArrayInputStream(sharedText.getBytes("UTF-8"));
         String filenameshort = "unknown";
         if(intent.getStringExtra(Intent.EXTRA_SUBJECT) != null) {
            filenameshort = intent.getStringExtra(Intent.EXTRA_SUBJECT);
         }
         String createdate = "";
         String corectiontogmt = "";
         // samotne odeslani dat
         result = SendRequest(is, filenameshort, createdate, corectiontogmt, serverIndex, tags);
     }
     catch (UnsupportedEncodingException e) {
         result = "Error: " + myshareservice.this.getString(R.string.msgUploadFailed) + " (text)";
         Log.e(this.getClass().getName(), e.toString());
      }
      return result;
   }
   
   private String mainSendRequest(Intent intent, Uri uri, int serverIndex, String[] tags, Boolean multiple) {
      String result = "Error: unknown";
      // Query gallery for camera picture via
      // Android ContentResolver interface
      ContentResolver cr = getContentResolver();
      InputStream is;
      String filename = getRealPathFromContentUri(uri);
      try {
         // zjisteni InputStream obrazku dle uri
         try {
            is = cr.openInputStream(uri);
         } catch (SecurityException e) {
               is = new FileInputStream (new File(filename));
            }
         // zjisteni, kdy byl soubor naposledy modifikovan ("vytvoren")
         String filenameshort = "unknown";
         String createdate = "";
         String corectiontogmt = "";
         //Log.i(this.getClass().getName(), "uri: " + uri);
         //Log.i(this.getClass().getName(), "filename: " + filename);
         if(filename != null) {
            filenameshort = uri.getLastPathSegment();
            File file = new File(filename);
            if(file.exists()) {
               filenameshort = file.getName();
               Log.i("file.getName()", filenameshort);
               long epochlastmodified=file.lastModified();
               SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
               sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
               createdate = sdf.format(new Date(epochlastmodified));
               corectiontogmt = Integer.toString(0); // time is sent in GMT, therefore correction is always 0
               Log.i(this.getClass().getName(), "createdate: " + createdate);
            }
         } else if (uri.toString().contains("mendhak.gpslogger")) {
            filenameshort = uri.getLastPathSegment();
         }
         if(!multiple) {
            // davam prednost jmena z EXTRA_SUBJECT pred skutecnym jmenem souboru
            // (je to dobre pri posilani obrazku ci zvuku z aplikace Note Everything)
            if(intent.getStringExtra(Intent.EXTRA_SUBJECT) != null) {
               filenameshort = intent.getStringExtra(Intent.EXTRA_SUBJECT);
            }
         }
         // samotne odeslani dat
         result = SendRequest(is, filenameshort, createdate, corectiontogmt, serverIndex, tags);
      }
      catch (FileNotFoundException e) {
         result = "Error: " + myshareservice.this.getString(R.string.msgUploadFailed) + " (" + filename + ")"; // it would be nice to expand the notif to see even long paths
         Log.e(this.getClass().getName(), e.toString());
      }
      // Get binary bytes for encode
      return result;
   }

   private String SendRequest(InputStream is, String filenameshort, String createdate, String corectiontogmt, int serverIndex, String[] tags) {

      String result = "Error: unknown";

      /* reseni problemu s nepodarenou inicializaci httpclient
       *  - vetsinou pujde o chybu, ze nejsou dobre nacteny vlastni CA
       *    z lokalniho souboru */
      if(httpclient == null) {
         Log.e(this.getClass().getName(), "httpclient is null (may be problems with CA)");
         result = "Error: httpclient is null (may be problems with CA)";
         return result;
      }

      URL target_url = this.prefs.target_urls.get(serverIndex);
      String http_user = this.prefs.http_users.get(serverIndex);
      String http_password = this.prefs.http_passwords.get(serverIndex);
      Log.w(this.getClass().getName(), "http_user: "+http_user);
      HttpPost httppost = new HttpPost(target_url.toString());
      if (http_user != null && http_password != null) {
         String http_userpassword = http_user + ":" + http_password;
         httppost.setHeader("Authorization", "basic " + Base64.encodeToString(http_userpassword.getBytes(),Base64.NO_WRAP));
      }

      HttpResponse response;

      try {
         String serverinfo = "Error: no message from the server";
         ContentType textUTF8 = ContentType.create("text/plain", Charset.forName("UTF-8"));
         MultipartEntityBuilder builder = MultipartEntityBuilder.create();
         builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
         // builder.setCharset(StandardCharsets.UTF_8); supported in android api19+
         builder.addPart("img", new InputStreamBody(is, filenameshort)); // take by se zdal uvest mimetyp (pred filenameshort)
         builder.addPart("createdate", new StringBody(createdate, textUTF8));
         builder.addPart("corectiontogmt", new StringBody(corectiontogmt, textUTF8));

         if (this.prefs.prefer_exif_date) {
            builder.addPart("preferexifdate", new StringBody("1", textUTF8));
         } else {
            builder.addPart("preferexifdate", new StringBody("0", textUTF8));
         }
         for (int tagnum = 0; tagnum < tags.length; ++tagnum) {
            builder.addPart("tag", new StringBody(tags[tagnum], textUTF8));         
         }

         HttpEntity mpEntity = builder.build();
         httppost.setEntity(mpEntity);
         response = httpclient.execute(httppost);
         HttpEntity entity = response.getEntity();
         if (entity != null) {
            serverinfo=EntityUtils.toString(entity); // tohle muze asi pripadne nahradit radek nize (pokud budes chti s obsahem co je vracen pracovat)
            if(serverinfo.length()>100) {
               serverinfo=serverinfo.substring(0,100);
            }
            entity.getContent().close();
         }
         result = serverinfo;
      }
      catch (ClientProtocolException e) {
         Log.e(this.getClass().getName(), "ClientProtocolException: " + e.toString());
         result = "Error: ClientProtocolException";
      }
      catch (IOException e) {
         Log.e(this.getClass().getName(), "IOException: " + e.toString());
         result = "Error: " + myshareservice.this.getString(R.string.msgUploadFailed);
      }
      return result;
   }

   private void setNotif(int id, int maxid, String text, int type) {
      int idnotif = 0;
      int icon = R.drawable.notif_icon;
      int flag = Notification.FLAG_ONGOING_EVENT;
      if(type==1) {
         if(text.length()<2 || text.substring(0,2).compareTo("OK")!=0) {
            ++this.sendError;
            // pridavnou Error chybu v notify prihazuji jen pri odesilani vice jak jednoho dokumentu
            if(maxid!=0) { 
               setNotif(id, maxid, text, -1);
            }
         }
         else {
            ++this.sendOK;
         }
         if(maxid!=0) {
            text = Integer.toString(id+1) + "/" + Integer.toString(maxid+1) + " " + text;
         }
      }
      else if(type==-1) { 
         idnotif = id+1;
         icon = R.drawable.notif_icon_failed;
         flag = Notification.FLAG_AUTO_CANCEL;      
         if(maxid!=0) {
            text = Integer.toString(id+1) + "/" + Integer.toString(maxid+1) + " " + text;
         }
      }
      else if(type==2) {
         // neni chyba a byl odeslan jeden ci vice dokumentu (ve finale je tedy jen jedna notify zprava - tahle)
         if(this.sendError==0 && this.sendOK>0) {
            icon = R.drawable.notif_icon_success;
         }
         // je chyba a byl odesilan jen jeden dokument (ve finale je tedy jen jedna notify zprava - tahle)
         else if(this.sendError>0 && (this.sendOK+this.sendError)==1) {
            icon = R.drawable.notif_icon_failed;
         }
         // dokumentu bylo odesilano vice -> zprava z posledniho vysledku
         // bude nahrazena zpravou souhrnneho charakteru
         if((this.sendOK+this.sendError)>1) {
            text = myshareservice.this.getString(R.string.msgUploadEnd);
            text += " (";
            if(this.sendError>1) {
               text += "Error:" + Integer.toString(this.sendError) + "x, ";
            }
            text += "OK:" + Integer.toString(this.sendOK) + "x";
            text += ")";
         }
         flag = Notification.FLAG_AUTO_CANCEL;      
      }

      Notification.Builder builder = new Notification.Builder(this);
      builder.setTicker(null); // neuvadim zadny text, aby zbytecne na status baru nebyly sachry
      builder.setContentTitle("MyShare");
      builder.setContentText(text);
      builder.setSmallIcon(icon);
      builder.setContentIntent(notintent);
      if (Build.VERSION.SDK_INT >= 26) {  // from API 26+ is necessary to create NotificationChannel to be able to show notifs
         NotificationManager notifManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
         NotificationChannel channel = new NotificationChannel("myshare", "MyShare", NotificationManager.IMPORTANCE_DEFAULT);
         notifManager.createNotificationChannel(channel);
         builder.setChannelId("myshare");
      }
      if (text.length() > 38)
         builder.setStyle(new Notification.BigTextStyle().bigText(text));
      Notification notif = builder.build();
      notif.flags = flag;
      this.notman.notify(idnotif, notif);
   }

   // Convert the image/file URI to the direct file system path of the file
   public String getRealPathFromContentUri(Uri contentUri) {
      String filePath = null;
      try {
         if (contentUri.getScheme().compareTo("content") == 0) {
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(contentUri, filePathColumn, null, null, null);
            if (cursor != null) {
               cursor.moveToFirst();
               int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
               if (columnIndex == -1) {
                  return filePath; // null
               }
               filePath = cursor.getString(columnIndex);
               cursor.close();
            }
         } else if (contentUri.getScheme().compareTo("file") == 0) {
            filePath = contentUri.getPath();
         }
      } catch (SecurityException e) {
         if (contentUri.toString().contains("mendhak.gpslogger")) {   // ugly hack just for GPSLogger - we lose permissions when our activity
            String storage = Environment.getExternalStorageDirectory().toString();  // finishes and wakes up another -> SEND_MULTIPLE only??!
            filePath = storage + "/Android/data/com.mendhak.gpslogger/files/" + contentUri.getLastPathSegment();
         }
      }
      return filePath;
   }

}
