package net.chlup.myshare;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import net.chlup.myshare.checklistadapter.ViewHolder;

import android.app.Activity;
import android.content.Intent;
import android.content.res.XmlResourceParser;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

public class mysharetag extends Activity {

   /* Prefs */
   Preferences prefs;
   ArrayAdapter<checklistmodel> adapter = null;

   /* Storage PATH */
   String storagepath = Environment.getExternalStorageDirectory().toString();
   String tagsfilename = storagepath + "/myshare/tags.xml";

   /** Called when the activity is first created. */
   @Override
   public void onCreate(Bundle savedInstanceState) {
      /* Get prefs */
      this.prefs = new Preferences(this);
      this.prefs.load();

      if (!this.prefs.use_default_theme) {
         if (this.prefs.dark_mode) {
            setTheme(android.R.style.Theme_Material);
         } else {
            setTheme(android.R.style.Theme_Material_Light);
         }
      }

      super.onCreate(savedInstanceState);
      setContentView(R.layout.mysharetag);

      if(this.prefs.target_urls.get(0) != null) {
        Button btnSend = (Button) findViewById(R.id.btnSend);
        btnSend.setOnClickListener(btnSendListener);
        File file = new File(tagsfilename);
        /* vyber serveru */
        Spinner serverSpin = (Spinner)findViewById(R.id.serverSpin);
        ArrayList<String> serverStrList = new ArrayList<String>();
        for (int i=0; i<this.prefs.target_urls.size(); ++i) {
           String serverText = this.prefs.target_urls.get(i).getHost();
           if(this.prefs.http_users.get(i) != null) {
              serverText += " - " + this.prefs.http_users.get(i);
           }
           serverStrList.add(serverText);
        }
        ArrayAdapter<String> serverAdapter = new ArrayAdapter<String>(
              this,
              android.R.layout.simple_spinner_item, serverStrList
        );
        serverAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        serverSpin.setAdapter(serverAdapter);
        // na vyber je jen jeden server -> zneviditelni widget tak, ze ani nezabira zadne misto
        if(this.prefs.target_urls.size()==1) {
           serverSpin.setVisibility(View.GONE);
        }
        /* vyber znacek */
        if(!file.exists()) {
            Toast toast = Toast.makeText(
                    getApplicationContext(),
                    getString(R.string.textFileTagsNotFound) + " (" + tagsfilename + ")!",
                    Toast.LENGTH_LONG
              );
              toast.show();
        }
        // zobrazeni znacek - z uzivatelova souboru nebo z vestaveneho souboru v aplikaci (R.raw.tags) - viz funkce getModel
        adapter = new checklistadapter(this, getModel(file.exists()));
        ListView tagList = (ListView)findViewById(R.id.tagList);
        //tagList.setItemsCanFocus(false);
        //tagList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        tagList.setAdapter(adapter);
        // When item is tapped, toggle checked properties of CheckBox and Planet.  
        tagList.setOnItemClickListener(new AdapterView.OnItemClickListener() {  
           @Override  
           public void onItemClick(AdapterView<?> parent, View checklistView, int position, long id) {
              ViewHolder holder = (ViewHolder) checklistView.getTag();
              holder.checkbox.setChecked(!holder.checkbox.isChecked());
           }  
        });
     }
     else {
        this.finish();
     }
   }

   private List<checklistmodel> getModel(boolean fileExists) {
      List<checklistmodel> list = new ArrayList<checklistmodel>();
      try {
         XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
         XmlPullParser tagsXml = factory.newPullParser();
         if(fileExists) {
        	 FileReader tagsXmlF = new FileReader(tagsfilename);
        	 tagsXml.setInput(tagsXmlF);
         }
         else {
        	 InputStream tagsXmlI = this.getResources().openRawResource(R.raw.tags);
        	 tagsXml.setInput(tagsXmlI, "UTF_8");
         }
         int eventType = -1;
         while (eventType != XmlResourceParser.END_DOCUMENT) {
            if (eventType == XmlResourceParser.START_TAG) {
               String strNode = tagsXml.getName();
               if (strNode.equals("tag")) {
                  list.add(
                        get(
                              tagsXml.getAttributeValue(null, "label"),
                              tagsXml.getAttributeValue(null, "sublabel")
                        )
                  );
                  if(
                        tagsXml.getAttributeValue(null, "selected") != null &&
                        (
                              tagsXml.getAttributeValue(null, "selected").compareToIgnoreCase("1")==0 ||
                              tagsXml.getAttributeValue(null, "selected").compareToIgnoreCase("true")==0
                        )
                  ) {
                     list.get(list.size()-1).setSelected(true);
                  }
               }
            }
            eventType = tagsXml.next();
         }
      }
      catch (XmlPullParserException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
      catch (IOException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
      return list;
   }
   
   private checklistmodel get(String label, String sublabel) {
      return new checklistmodel(label, sublabel);
   }
   
   private OnClickListener btnSendListener = new OnClickListener() {
      public void onClick(View view) {
         Intent intent = getIntent();
         Bundle extras = intent.getExtras();
         String action = intent.getAction();
            
         if (Intent.ACTION_SEND.equals(action) || Intent.ACTION_SEND_MULTIPLE.equals(action)) {
            //Intent intentservice=new Intent(this, myshareservice.class);
            Intent intentservice=new Intent(view.getContext(), myshareservice.class);
            intentservice.setAction(action);
            intentservice.putExtras(extras);
            /* co bylo vybrano za server? */
            int serverIndex = 0;
            Spinner serverSpin = (Spinner)findViewById(R.id.serverSpin);
            if(serverSpin.getSelectedItemPosition() >= 0) {
               serverIndex = serverSpin.getSelectedItemPosition();
            }
            intentservice.putExtra("MYSHARE_SERVERINDEX", serverIndex);
            /* co bylo vybrano za znacky? */
            ArrayList<String> tagsArrayList = new ArrayList<String>();
            if(adapter != null) {
               for(int position=0; position<adapter.getCount(); ++position) {
                  checklistmodel element = adapter.getItem(position);
                  if(element.isSelected()) {
                     tagsArrayList.add(element.getSublabel());
                  }
               }
            }
            String[] tags = tagsArrayList.toArray(new String[tagsArrayList.size()]);
            intentservice.putExtra("MYSHARE_TAGS", tags);
            startService(intentservice);
         }
         finish();
      }
   };

}
