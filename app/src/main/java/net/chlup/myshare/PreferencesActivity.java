package net.chlup.myshare;

import android.preference.PreferenceActivity;
import android.os.Bundle;

/* The preference displaying and poking around activity */
public class PreferencesActivity extends PreferenceActivity {
   @Override
   protected void onCreate(Bundle savedInstanceState) {
      Preferences prefs = new Preferences(this);
      prefs.load();

      if (!prefs.use_default_theme) {
         if (prefs.dark_mode) {
            setTheme(android.R.style.Theme_Material);
         } else {
            setTheme(android.R.style.Theme_Material_Light);
         }
      }

      super.onCreate(savedInstanceState);
      addPreferencesFromResource(R.xml.preferences);
   }
}
