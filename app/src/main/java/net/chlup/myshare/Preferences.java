package net.chlup.myshare;

import android.preference.PreferenceManager;
import android.util.Log;
import android.content.Context;
import android.content.SharedPreferences;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class Preferences {
   Context ctx;

   /* The preference values */
   public ArrayList<URL> target_urls = new ArrayList<URL>();
   public ArrayList<String> http_users = new ArrayList<String>();
   public ArrayList<String> http_passwords = new ArrayList<String>();
   public boolean prefer_exif_date;
   public boolean own_key_store;
   public boolean use_default_theme;
   public boolean dark_mode;

   Preferences(Context ctx) {
      this.ctx = ctx;
   }

   public void load() {
      SharedPreferences prefs = PreferenceManager
            .getDefaultSharedPreferences(this.ctx);

      int serverIndex = -1;
      for (int i=0; i<3; ++i) {
         URL target_url;
         String iStr = "" + (i+1);
         String target_urlStr_default = "";
         if(i==0) {
            iStr = "";
            target_urlStr_default = "https://intra.chlup.net/MyVector/mysharesend.fcgi";
         }
         String target_urlStr = prefs.getString("target_url" + iStr, target_urlStr_default);
         if(target_urlStr.length()>1) {
            try {
               target_url = new URL(target_urlStr);
            }
            catch (MalformedURLException e) {
               Log.e(this.getClass().getName(), e.toString());
               target_url = null;
            }
            if(target_url != null) {
               ++serverIndex;
               this.target_urls.add(target_url);
               String http_user = prefs.getString("http_user" + iStr, null);
               if (http_user == null || http_user.length() < 1)
                  http_user = null;
               this.http_users.add(http_user);
               String http_password = prefs.getString("http_password" + iStr, null);
               if (http_password == null || http_password.length() < 1)
                  http_password = null;
               this.http_passwords.add(http_password);
            }
         }
      }
      if(serverIndex == -1) {
         this.target_urls.add(null);
         this.http_users.add(null);
         this.http_passwords.add(null);
      }

      this.prefer_exif_date = prefs.getBoolean("prefer_exif_date", true);
      this.own_key_store = prefs.getBoolean("own_key_store", false);
      this.use_default_theme = prefs.getBoolean("default_theme", true);
      this.dark_mode = prefs.getBoolean("dark_mode", false);

   }
}
