package net.chlup.myshare;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

public class checklistadapter extends ArrayAdapter<checklistmodel> {
   private final List<checklistmodel> list;
   private final Activity context;

   static class ViewHolder {
      protected TextView tvLabel, tvSublabel;
      protected CheckBox checkbox;
   }
   
   public checklistadapter(Activity context, List<checklistmodel> list) {
      super(context, R.layout.checklistlayout, list);
      this.context = context;
      this.list = list;
   }

   @Override
   public View getView(int position, View convertView, ViewGroup parent) {
      View checklistView = null;
      if (convertView == null) {
         LayoutInflater inflator = context.getLayoutInflater();
         checklistView = inflator.inflate(R.layout.checklistlayout, null);
         float scale = checklistView.getScaleX();
         checklistView.setPadding(checklistView.getPaddingLeft(), (int) checklistView.getPaddingTop() + (int)(8.0 * scale),
                 checklistView.getPaddingRight(), checklistView.getBottom() + (int)(8.0 * scale));
         final ViewHolder viewHolder = new ViewHolder();
         viewHolder.tvLabel = (TextView) checklistView.findViewById(R.id.label);
         viewHolder.tvLabel.setFocusable(false);
         viewHolder.tvLabel.setFocusableInTouchMode(false);
         viewHolder.tvSublabel = (TextView) checklistView.findViewById(R.id.sublabel);
         viewHolder.tvSublabel.setFocusable(false);
         viewHolder.tvSublabel.setFocusableInTouchMode(false);
         viewHolder.checkbox = (CheckBox) checklistView.findViewById(R.id.check);
         viewHolder.checkbox.setFocusable(false);
         viewHolder.checkbox.setFocusableInTouchMode(false);
         viewHolder.checkbox.setClickable(false);
         viewHolder.checkbox.setOnCheckedChangeListener(
               new CompoundButton.OnCheckedChangeListener() {
                  @Override
                  public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                     checklistmodel element = (checklistmodel) viewHolder.checkbox.getTag();
                     element.setSelected(buttonView.isChecked());
                     //System.out.println("Checked : " + element.getLabel() + " - " + element.isSelected() + " - " + element.getSublabel());
                  }
               }
         );
         checklistView.setTag(viewHolder);
         viewHolder.checkbox.setTag(list.get(position));
      }
      else {
         checklistView = convertView;
         ((ViewHolder) checklistView.getTag()).checkbox.setTag(list.get(position));
      }
      ViewHolder holder = (ViewHolder) checklistView.getTag();
      holder.tvLabel.setText(list.get(position).getLabel());
      holder.tvSublabel.setText(list.get(position).getSublabel());
      holder.checkbox.setChecked(list.get(position).isSelected());
      return checklistView;
   }
}
