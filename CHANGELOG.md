#### version 1.59
* fix bug when the first file from GPSLogger was "unknown"


#### version 1.58
* fix bug in cz localisation
* add back arrow from Settings


#### version 1.57
* add default theme option (now as default)
* remove preview window from MyShare with tags


#### version 1.56
* add cz localisation for DarkMode


#### version 1.55
* notification can be enlarged when text is not fully visible
* add Light/Dark theme switcher with Light as default


#### version 1.54
* patch SecurityException when multiple files are shared from GPSLogger
* update notification info when file does not exist (or missing permissions on uri)


#### version 1.52
* tune layout of MyShare with tags' checklist and select-server option
* add higher resolution icons for MyShare
* add higher resolution icons for MyShare with tags


#### version 1.51
* enlarge margins in checklist and spinner
* checkbox in checklist should be more centered
* hopefully fixing "freeze bug" while sending on non-reliable connection
* release builds are now automatically generated as myshare_<version name>.apk


#### version 1.50
* now using cz.msebera.android.httpclient because of dependecy problems
* notifiations working on API26+
* got rid of deprecated stuff
* bug fix
* now using the last segment of uri for a filename when real is not known


##### *for source codes of any previous versions see chlup.net*

#### version 1.42 
* Čt lis 13 15:04:12 CET 2014
*  mirne zvetseni pisma v Myshare for with tags

#### version 1.41
* Čt lis 13 13:57:14 CET 2014
* upravy, aby v Myshare for with tags nebyl v Androidu 4.4 cudl Odeslat prilis vysoko - pokus c. 2 (predtim to nevyslo)

#### version 1.40
* Čt lis 13 13:26:16 CET 2014
* upravy, aby v Myshare for with tags nebyl v Androidu 4.4 cudl Odeslat prilis vysoko
* najeti defaultnich znacek, pokud nema uzivatel vytvoren svuj soubor /sdcard/myshare/tags.xml

#### version 1.39
* Po zář 15 09:12:14 CEST 2014
* upravy pro sestaveni pod adt-bundle-linux-x86_64-20140702
* pridano pravo android.permission.READ_EXTERNAL_STORAGE nutne pro fungovani pod KitKat pri vlastnim ulozisiti certifikatu (a zrejme i pro "MyShare se znackami")
* oprava nedotazeni preddefinovaneho url z .../mysharesend.phtml na .../mysharesend.fcgi

#### version 1.38
* Pá pro 27 14:06:52 CET 2013
* upravy pro sestaveni pod adt-bundle-linux-x86_64-20131030
* zavedeni prepinace na preferovani Exif data - do HTTP POST doplnen "preferexifdate"
* zmena preddefinovaneho url z .../mysharesend.phtml na .../mysharesend.fcgi

#### version 1.37
* Pá srp 31 13:03:10 CEST 2012
* oprava, aby fungoval i treti voliteny server

#### version 1.36
* Pá srp 31 12:14:49 CEST 2012
* moznost dalsich dvou serveru kam odesilat, pro vyber je nutne uzit MyShare se znackami

#### version 1.35
* Po bře 12 17:50:24 CET 2012
* predelavka ikony hotovo a jina ikona k aktivite MyShare se znackami

#### version 1.34
* Po bře 12 17:10:13 CET 2012
* dalsi kosmetika v notifikacich

#### version 1.33
* Po bře 12 16:54:10 CET 2012
* nepridavam navic chybovou hlasku v notifikaci pokud jde o odesilani jedineho dokumentu

#### version 1.32
* Po bře 12 16:38:01 CET 2012
* vylepseni notifikaci (nepribyvani OK notifikaci a chybove notifikace maji spesl ikonu)

#### version 1.31
* Pá bře  9 13:55:34 CET 2012
* zmena v "MyShare se znackami" - nyni se v souboru /sdcard/myshare/tags.xml konfiguruje seznam znacek a uzivatel si pak mezi temito znackami vybira (priklad viz myshare/tags.xml)

#### version 1.30
* Čt bře  8 09:05:32 CET 2012
* v poznamce zdrojaku pridan kod jak mozno backportovat na Adnrodid 2.1 (zalezitos kolem Base64)
* zruseno 'android:screenOrientation="portrait"' coz zpusobovalo rusivou rotaci pri odesilani u Asus Transformer s ICS 4 pri orientaci na sirku

#### version 1.29
* So úno 18 19:02:26 CET 2012
* oprava pri zasilani souboru, ktere maji v nazvu souboru diakritiku (nyni by mela diakrtitika v nazvu pekne v UTF-8 dojit)

#### version 1.28
* So úno 18 16:50:35 CET 2012
* vylepseni pri zasilani napriklad z Note Everything - davam nyni prednost retezci Intent.EXTRA_SUBJECT (kde je nazev poznamky v Note Everything) pred skutecnym nazvem souboru (ktery je napriklad voice8.3gp coz nic nerika)

#### version 1.27
* So úno 18 15:42:22 CET 2012
* schopnost posilat data predavana jako text (nyni spracuje Intent.EXTRA_TEXT)

#### version 1.26
* Pá pro 30 18:31:55 CET 2011
* rozpoznavani skutecne cesty i k jinym souborum nez k obrazkum a videim, coz je vhodne pro odesilani libovolnych souboru napriklad z ES Spravce Souboru 

#### version 1.25
* Po pro  5 15:25:48 CET 2011
* pri zahajeni nahravani to tak nerajtuje v status baru
* povoleny vsechny mimetypy (aspon doufam)
* pridana moznost MyShare se znackami

#### version 1.24
* Pá říj 28 16:13:07 CEST 2011
* pridana moznost posilat video soubory (<data android:mimeType="video/*" />)

#### version 1.23
* Čt říj 20 17:28:44 CEST 2011
* zajisteni, ze nedojde k pokusu o http konekci pokud nejsou dobre nacteny certifikaty z vlastniho souboru s CA (za predpokladu, ze je zatrzena volba "Vlastni uloziste klicu")

#### version 1.22
* St říj 19 13:40:43 CEST 2011
* oprava prednastaveneho URL pro zasilani obrazku
* St říj 19 13:19:34 CEST 2011
* pri uziti vlastnich certifikacnich autorit ze souboru se to hroutilo, jelikoz jsi ve verzi 1.21 zavedl novy zpusob zapisu definice Scheme; nevim proc se to hrouti - po vraceni stareho zpusobu (ktery je v novem httpclient oznacen za deprecated) to vsak funguje

#### version 1.21
* St říj 19 12:03:32 CEST 2011
* prechod od zasilani obrazku ve forme retezce Base64 na POST zasilani souboru (binarne) pomoci MultipartEntity
  + Pozitiva:
    - radikalni zmenseni zasilanych dat temer o 30% diky binarni podobe
  + Negativa:
    - MultipartEntity vyzaduje vlozeni externich knihoven "HttpClient 4.1.2 (GA)" (httpcomponents-client-4.1.2), jelikoz verze HttpClient co je v Androidu to neumi
    - cely apk balik diky vlozene externi knihovne naroste z asi 26kB na asi 294kB

#### version 1.20
* St říj 19 09:26:14 CEST 2011
* konstanta /sdcard nahrazena promenou storagepath
* snaha zvladnout nenalezeni fyzicke cesty k soubotu obrazku - asi k tomu muze dochazet pri spusteni share z neceho jineho nez z galerii

* St říj 19 09:09:56 CEST 2011
* ve zdrojovem kodu nahrazeny taby za mezery

#### version 1.19
* St říj  5 16:52:35 CEST 2011
* screenOrientation: portrait

#### version 1.18
* St říj  5 15:50:17 CEST 2011
* novinky kolem casu poseldni modifikace nebyly dobre - tahle verze je opravuje; cas modifikace take posila v GMT a informuje o tom i zaslanim informace, ze posun zaslaneho casu od GMT je 0

#### version 1.15
* St říj  5 11:53:18 CEST 2011
* duplicitni kod kolem posilani jednoho a vice obrazku sjednocen do jedne funkce
* u obrazku je nyni odesilana informace o jeho posledni modifikaci ("jeho vytvoreni")

#### version 1.14
* Út říj  4 12:30:17 CEST 2011
* kratsi texty do status baru

#### version 1.13
* Po říj  3 21:08:22 CEST 2011
* upload pokracuje i pri zamcni telefonu (s vypnutou obrazovkou) - je zakazan prechod do rezimu spanku ro CPU dokud neni upload dokoncen
* lepsi procisteni predeslych notifikacnich zprav

#### version 1.10
* Po říj  3 16:43:16 CEST 2011
* prechod na service (IntentService)
* hlaseni o nahravkach v notifikacni liste
* !!! je to zatim hodne neozkousena beta
  - pokud to nebude zcela fungovat ci treba to bede zrat po uziti v delsim horizontu baterku - tak zvaz navrat k verzi 1.08 !!!

#### version 1.08
* Ne říj  2 15:47:39 CEST 2011
* moznost uploadu vice souboru naraz

