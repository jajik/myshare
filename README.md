MyShare
Please see the file LICENSE for license information
===================================================

Disclaimer (see LICENSE for details)
------------------------------------
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

Setting up the code
-------------------
The project is now based on the Android build system plugin for Gradle. You
can easily use *Import project (Gradle, Eclipse ADT, etc.) in Android Studio
and everything should work fine.

